import logging

import requests

from cryo_agent_api_client.models import CryoAgentLatestData, CryoAgentQueuedData

LOGGER = logging.getLogger(__name__)


class CryoAgentApiClient:
    def __init__(self, url: str) -> None:
        self._url = url

    def get_queued_data(self) -> CryoAgentQueuedData:
        """
        Function to get the queued data from an agent
        """

        # Get data from endpoint. If time-out, return None
        res = self._get_data(self._url + "/queued/")
        if not res:
            return None

        # Parse response data and return the CryoAgentResponseData object
        return CryoAgentQueuedData.create_from_json(res.json())

    def get_latest_data(self) -> CryoAgentLatestData:
        """
        Function to get data from an agent at given url
        """

        # Get data from endpoint. If time-out, return None
        res = self._get_data(self._url + "/latest/")
        if not res:
            return None

        # Parse response data and return the CryoAgentResponseData object
        return CryoAgentLatestData.create_from_json(res.json())

    @staticmethod
    def _get_data(url: str):
        # Get data from endpoint. If time-out, return None
        try:
            res = requests.get(url)
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError) as exc:
            LOGGER.error(exc, exc_info=True)
            return None

        # If no error, simply return the result
        return res

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass
