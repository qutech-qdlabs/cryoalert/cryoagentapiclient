from cryo_agent_api_client.api import CryoAgentApiClient


def _timing_test():
    with CryoAgentApiClient("http://localhost:8081") as client:
        client.get_latest_data()


if __name__ == "__main__":
    import cProfile
    import pstats

    profiler = cProfile.Profile()
    profiler.enable()
    _timing_test()
    profiler.disable()
    stats = pstats.Stats(profiler).sort_stats("tottime")
    stats.print_stats()
