from dataclasses import dataclass
from datetime import datetime
from typing import Any, Dict, List

from typing_extensions import Self


@dataclass
class CryoAgentDataPoint:
    timestamp: datetime
    unit: str
    value: float
    var_name: str
    meta: Dict[str, Any]

    @classmethod
    def create_from_json(cls, data_json: Dict[str, Any]) -> Self:
        """
        Classmethod to easily construct this class from a json list
        """

        # Return itself
        return cls(
            timestamp=data_json["timestamp"],
            unit=data_json["unit"],
            value=data_json["value"],
            var_name=data_json["var_name"],
            meta=data_json.get("meta", {}),
        )


@dataclass
class CryoAgentDataList:
    data: List[CryoAgentDataPoint]
    timestamp: datetime

    @classmethod
    def create_from_json(cls, data_json: List[Dict[str, Any]]) -> Self:
        """
        Classmethod to easily construct this class from a json list
        """

        # Create datapoints
        dps = [CryoAgentDataPoint.create_from_json(dp) for dp in data_json["data"]]

        # Return itself
        return cls(data=dps, timestamp=data_json["timestamp"])


@dataclass
class CryoAgentQueuedData:
    data: List[CryoAgentDataList]
    name: str
    timestamp: datetime

    @classmethod
    def create_from_json(cls, data_json) -> Self:
        """
        Classmethod to easily construct this class from a json list
        """

        # Create data lists
        data_lists = [CryoAgentDataList.create_from_json(data_list) for data_list in data_json["data"]]

        # Return itself
        return cls(data=data_lists, name=data_json["name"], timestamp=data_json["timestamp"])


@dataclass
class CryoAgentLatestData:
    data: CryoAgentDataList
    name: str
    timestamp: datetime

    @classmethod
    def create_from_json(cls, data_json) -> Self:
        """
        Classmethod to easily construct this class from a json list
        """

        # Create data lists
        data_list = CryoAgentDataList.create_from_json(data_json["data"])

        # Return itself
        return cls(data=data_list, name=data_json["name"], timestamp=data_json["timestamp"])
