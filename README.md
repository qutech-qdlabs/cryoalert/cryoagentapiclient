# CryoAgent API Client

This package implements a simple API client for the HTTP interface of the cryo-agent application

## Installation


```
> poetry source add gitlab-tudelft https://gitlab.tudelft.nl/api/v4/projects/7052/packages/pypi/simple
> poetry add --source gitlab-tudelft cryo_agent_api_client
```
Note that `poetry source` is only available from poetry version 1.2.

## Usage 

```python 
from cryo_agent_api_client.api import CryoAgentApiClient
from cryo_agent_api_client.models import CryoAgentQueuedData, CryoAgentLatestData

with CryoAgentApiClient(url="http://<ip>:<port>") as client:
    queued_data: CryoAgentQueuedData = client.get_queued_data()
    latest_data: CryoAgentLatestData =  client.get_latest_data()
```

## Changelog

### **Version 1.0**

- Initial Release

### **Version 1.1**

- Make API Client python 3.7 compatible

### **Version 1.2**

- Make compatible with cryo-agent version 0.4